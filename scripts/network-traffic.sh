#!/bin/bash

# from https://github.com/polybar/polybar-scripts/blob/master/polybar-scripts/network-traffic/network-traffic.shi

print_bytes() {
    if [ "$1" -eq 0 ] || [ "$1" -lt 1000 ]; then
        bytes="$(printf "%5d" $1)  B/s"
    elif [ "$1" -lt 1000000 ]; then
        bytes="$(printf "%5.1f" $(echo "scale=0;$1/1000" | bc -l)) kB/s"
    else
        bytes="$(printf "%5.1f" $(echo "scale=1;$1/1000000" | bc -l)) MB/s"
    fi

    echo "$bytes"
}

print_bit() {
    if [ "$1" -eq 0 ] || [ "$1" -lt 10 ]; then
        bit="0 B"
    elif [ "$1" -lt 100 ]; then
        bit="$(echo "scale=0;$1*8" | bc -l ) B"
    elif [ "$1" -lt 100000 ]; then
        bit="$(echo "scale=0;$1*8/1000" | bc -l ) K"
    else
        bit="$(echo "scale=1;$1*8/1000000" | bc -l ) M"
    fi

    echo "$bit"
}

INTERVAL=3

declare -A bytes

while true; do
    down=0
    up=0

    # Get active connected interfaces, type and ssids
    readarray -t interfaces -s 1 <<< "$(nmcli --fields device connection show --active)"
    readarray -t types -s 1 <<< "$(nmcli --fields type connection show --active)"
    readarray -t ssids -s 1 <<< "$(nmcli --fields name connection show --active)"
    interfaces="${interfaces[@]:1}"
    types="${types[@]:1}"
    ssids="${ssids[@]:1}"
    
    # Get total traffic
    for interface in $interfaces; do
        
        # In case interface was not up before, we need to set this
        if [ -z ${bytes[past_rx_$interface]} ]; then
            bytes[past_rx_$interface]=0
        fi
        if [ -z ${bytes[past_tx_$interface]} ]; then
            bytes[past_tx_$interface]=0
        fi

        bytes[now_rx_$interface]="$(cat /sys/class/net/"$interface"/statistics/rx_bytes 2>/dev/null || echo -2)"
        bytes[now_tx_$interface]="$(cat /sys/class/net/"$interface"/statistics/tx_bytes 2>/dev/null || echo -2)"

        bytes_down=$((((${bytes[now_rx_$interface]} - ${bytes[past_rx_$interface]})) / $INTERVAL))
        bytes_up=$((((${bytes[now_tx_$interface]} - ${bytes[past_tx_$interface]})) / $INTERVAL))

        down=$(((( "$down" + "$bytes_down" ))))
        up=$(((( "$up" + "$bytes_up" ))))

        bytes[past_rx_$interface]=${bytes[now_rx_$interface]}
        bytes[past_tx_$interface]=${bytes[now_tx_$interface]}
    done

    # get links' name
    names=
    declare -a names
    for i in $(seq $((${#types[@]} - 1))); do
        if [ ${types[$i]} == ethernet ]; then
            names[$(($i - 1))]=Ethernet
        elif [ ${types[$i]} == wifi ]; then
            names[$(($i - 1))]=${ssids[$i]}
        else
            names[$(($i - 1))]=Unknown
        fi
    done

    echo "${names[@]/% */} | : $(print_bytes $down) | : $(print_bytes $up)"
    # echo "Download: $(print_bit $down) / Upload: $(print_bit $up)"

    sleep $INTERVAL
done
